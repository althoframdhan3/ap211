package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LogController.class)
class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    private Mahasiswa mahasiswa;
    private MataKuliah matkul;
    private Log log;

    private String start = "01-01-2021 10:00:00";
    private String end = "01-01-2021 11:00:00";
    private String deskripsi = "test";
    private Map<String, String> json;

    @BeforeEach
    void setUp() throws ParseException {
        mahasiswa = new Mahasiswa(
                "1906192052",
                "Maung Meong",
                "maung@cs.ui.ac.id",
                "4",
                "081317691718");

        Date startTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(start);
        Date endTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(end);

        log = new Log(mahasiswa, startTime, endTime, deskripsi);

        json = new HashMap<>();
        json.put("start", start);
        json.put("end", end);
        json.put("deskripsi", deskripsi);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerpostLog() throws Exception {
        when(mahasiswaService.createMahasiswa(mahasiswa)).thenReturn(mahasiswa);

        mvc.perform(post("/log/mhs/"+mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(json)))
                .andExpect(status().isOk());

    }

    @Test
    void testControllergetListLog() throws Exception {
        Iterable<Log> listLog = Arrays.asList(log);
        when(logService.getListLog()).thenReturn(listLog);

        mvc.perform(get("/log").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    void testControllergetLogMahasiswa() throws Exception {
        when(mahasiswaService.createMahasiswa(mahasiswa)).thenReturn(mahasiswa);

        mvc.perform(get("/log/mhs/"+mahasiswa.getNpm()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    void testControllergetReportMahasiswa() throws Exception {
        when(mahasiswaService.createMahasiswa(mahasiswa)).thenReturn(mahasiswa);

        mvc.perform(get("/log/mhs/"+mahasiswa.getNpm()+"/report").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    void testControllerupdateLog() throws Exception {
        when(logService.createLog(mahasiswa.getNpm(), json)).thenReturn(log);

        mvc.perform(put("/log/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(json)))
                .andExpect(status().isOk());
    }

    @Test
    void testControllerdeleteLog() throws Exception {
        when(logService.createLog(mahasiswa.getNpm(), json)).thenReturn(log);
        mvc.perform(delete("/log/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }
}