package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MahasiswaServiceImplTest {
    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @InjectMocks
    private MahasiswaServiceImpl mahasiswaService;

    private Mahasiswa mahasiswa;

    @Mock
    private MataKuliahRepository matakuliahRepository;

    @InjectMocks
    private MataKuliahServiceImpl matakuliahService;

    private MataKuliah matkulku;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");

        matkulku = new MataKuliah();
        matkulku.setKodeMatkul("ADVPROG");
        matkulku.setNama("Advanced Programming");
        matkulku.setProdi("Ilmu Komputer");

        mahasiswaRepository.save(mahasiswa);
        matakuliahRepository.save(matkulku);
    }

    @Test
    public void testServiceCreateMahasiswa(){
        lenient().when(mahasiswaService.createMahasiswa(mahasiswa)).thenReturn(mahasiswa);
    }

    @Test
    public void testServiceGetListMahasiswa(){
        Iterable<Mahasiswa> listMahasiswa = mahasiswaRepository.findAll();
        lenient().when(mahasiswaService.getListMahasiswa()).thenReturn(listMahasiswa);
        Iterable<Mahasiswa> listMahasiswaResult = mahasiswaService.getListMahasiswa();
        Assertions.assertIterableEquals(listMahasiswa, listMahasiswaResult);
    }

    @Test
    public void testServiceGetMahasiswaByNpm(){
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        Mahasiswa resultMahasiswa = mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm());
        assertEquals(mahasiswa.getNpm(), resultMahasiswa.getNpm());
    }

    @Test
    public void testServiceDeleteMahasiswa(){
        mahasiswaService.createMahasiswa(mahasiswa);
        mahasiswaService.deleteMahasiswaByNPM("1906192052");
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(null);
        assertNull(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm()));
    }

    @Test
    public void testServiceDeleteMahasiswaAsdosMatkulIsNull(){
//        mahasiswaService.createMahasiswa(mahasiswa);
//        mahasiswaService.deleteMahasiswaByNPM("1906192052");
        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm()))
                .thenReturn(mahasiswa);
        mahasiswaService.deleteMahasiswaByNPM(mahasiswa.getNpm());
        assertNotNull(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm()));
    }

    @Test
    public void testServiceDeleteMahasiswaAsdosMatkulNotNull(){
//        mahasiswaService.createMahasiswa(mahasiswa);
//        mahasiswaService.deleteMahasiswaByNPM("1906192052");
        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm()))
                .thenReturn(mahasiswa);
        lenient().when(matakuliahRepository.findByKodeMatkul(matkulku.getKodeMatkul()))
                .thenReturn(matkulku);
        mahasiswaService.setMataKuliah(mahasiswa.getNpm(), matkulku.getKodeMatkul());

        mahasiswaService.deleteMahasiswaByNPM(mahasiswa.getNpm());
        assertNotNull(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm()));
    }

    @Test
    public void testServiceUpdateMahasiswa(){
        mahasiswaService.createMahasiswa(mahasiswa);
        String currentIpkValue = mahasiswa.getIpk();
        //Change IPK from 4 to 3
        mahasiswa.setIpk("3");

        lenient().when(mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa))
                .thenReturn(mahasiswa);
        Mahasiswa resultMahasiswa = mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa);

        assertNotEquals(resultMahasiswa.getIpk(), currentIpkValue);
        assertEquals(resultMahasiswa.getNama(), mahasiswa.getNama());


    }

    @Test
    public void testServiceSetMataKuliahSuccess(){

        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm()))
                .thenReturn(mahasiswa);
        lenient().when(matakuliahRepository.findByKodeMatkul(matkulku.getKodeMatkul()))
                .thenReturn(matkulku);
        mahasiswaService.setMataKuliah(mahasiswa.getNpm(), matkulku.getKodeMatkul());


        assertNotNull(mahasiswa.getAsdosMatKul());
    }

    @Test
    public void testServiceSetMataKuliahMatkulNull(){

        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm()))
                .thenReturn(mahasiswa);
        lenient().when(mahasiswaService.setMataKuliah(mahasiswa.getNpm(), matkulku.getKodeMatkul()))
                .thenReturn(null);

        assertNull(mahasiswa.getAsdosMatKul());
    }

    @Test
    public void testServiceSetMataKuliahMhsNull(){

        lenient().when(matakuliahRepository.findByKodeMatkul(matkulku.getKodeMatkul()))
                .thenReturn(matkulku);
        lenient().when(mahasiswaService.setMataKuliah(mahasiswa.getNpm(), matkulku.getKodeMatkul()))
                .thenReturn(null);

        assertNull(mahasiswa.getAsdosMatKul());
    }

}
