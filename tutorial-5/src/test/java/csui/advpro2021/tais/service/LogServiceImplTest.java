package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.assertj.core.condition.AnyOf;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.shadow.com.univocity.parsers.common.IterableResult;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.lenient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class LogServiceImplTest {

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @InjectMocks
    private MahasiswaServiceImpl mahasiswaService;

    private Mahasiswa mahasiswa;


    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private LogServiceImpl logService;

    private Log log;

    private Map<String, String> json = new HashMap<>();

    @BeforeEach
    void setUp() throws ParseException {

        json.put("start", "01-01-2021 10:00:00");
        json.put("end", "01-01-2021 11:00:00");
        json.put("deskripsi", "test");

        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");


        Date startTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(json.get("start"));
        Date endTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(json.get("end"));

        log = new Log();
        log.setId(1L);
        log.setMahasiswa(mahasiswa);
        log.setStart(startTime);
        log.setEnd(endTime);
        log.setDeskripsi(json.get("deskripsi"));

    }

    @Test
    public void testServiceCreateLogSuccess() throws Exception {
        Log dummy;
        lenient().when(mahasiswaRepository.findById(mahasiswa.getNpm())).thenReturn(Optional.of(mahasiswa));
        dummy = logService.createLog(mahasiswa.getNpm(), json);
        assertNotEquals(null, dummy);
    }

//    @Test
//    void createLog() {
//    }

//    @Test
//    void addHoursToJavaUtilDate() {
//    }

    @Test
    void TestgetLogById() {
        lenient().when(logService.getLogById(log.getId()))
                .thenReturn(Optional.ofNullable(log));
    }

    @Test
    void TestupdateLog() throws ParseException {
        lenient().when(logRepository.findById(log.getId()))
                .thenReturn(Optional.ofNullable(log));
        lenient().when(logService.updateLog(log.getId(), json))
                .thenReturn(log);

        assertNotNull(log);
        assertNotNull(logRepository.findById(log.getId()));
    }

    @Test
    void TestupdateLogReturnNull() throws ParseException {

        lenient().when(logService.updateLog(log.getId(), json))
                .thenReturn(null);

        assertNull(logRepository.findById(log.getId()));
    }

    @Test
    void TestdeleteLogById() {
        logService.deleteLogById(log.getId());
    }

    @Test
    void TestgetListLog() {
        lenient().when(logService.getListLog())
                .thenReturn(new ArrayList<>());
    }

    @Test
    void TestgetListLogByNpmSuccess() {

        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm()))
                .thenReturn(mahasiswa);

        lenient().when(logService.getListLogByNpm(mahasiswa.getNpm()))
                .thenReturn(mahasiswa.getLogList());
    }

    @Test
    void TestgetListLogByNpmReturnNull() {



        lenient().when(logService.getListLogByNpm(mahasiswa.getNpm()))
                .thenReturn(null);
    }

    @Test
    void TestgetReportSuccess() throws ParseException {

        Iterable<Map<String, Object>> dummy;
        List<Log> listLog;

        listLog = new ArrayList<>();

        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm()))
                .thenReturn(mahasiswa);

        Log dum = logService.createLog(mahasiswa.getNpm(), json);
        listLog.add(dum);
        mahasiswa.setLogList(listLog);

        dummy = logService.getReport(mahasiswa.getNpm());
        assertNotNull(dummy);

    }

    @Test
    void TestgetReportFail() throws ParseException {

        Iterable<Map<String, Object>> dummy;
        List<Log> listLog;

        listLog = new ArrayList<>();

        lenient().when(mahasiswaRepository.findByNpm(mahasiswa.getNpm()))
                .thenReturn(mahasiswa);

        Log dum = logService.createLog(mahasiswa.getNpm(), json);

        dummy = logService.getReport(mahasiswa.getNpm());
        assertNull(dummy);

    }
}