package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogService;

import csui.advpro2021.tais.service.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Map;

@RestController
@RequestMapping(path = "/log")
public class LogController {

    @Autowired
    private LogService logService;

    @Autowired
    private MahasiswaService mahasiswaService;

    @PostMapping(path = "/mhs/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postLog(@PathVariable(value = "npm") String npm, @RequestBody Map<String, String> json) throws ParseException {
        return ResponseEntity.ok(logService.createLog(npm, json));
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getListLog() {
        return ResponseEntity.ok(logService.getListLog());
    }
//
    @GetMapping(path = "/mhs/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLogMahasiswa(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getListLogByNpm(npm));
    }

    @GetMapping(path = "/mhs/{npm}/report", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getReportMahasiswa(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getReport(npm));
    }
//
    @PutMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@PathVariable(value = "id") Long id, @RequestBody Map<String, String> json) throws ParseException {
        return ResponseEntity.ok(logService.updateLog(id, json));
    }
//
    @DeleteMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "id") Long id) {
        logService.deleteLogById(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
