package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "npm_mahasiswa", referencedColumnName = "npm")
    @JsonIgnore
    private Mahasiswa mahasiswa;

//    public String getMahasiswa() {
//        return mahasiswa.getNama();
//    }

    @Column(name = "start_time")
    private Date start;

    @Column(name = "end_time")
    private Date end;

    @Column(name = "deskripsi")
    private String deskripsi;


    public Log(Mahasiswa mahasiswa, Date start, Date end, String deskripsi) {
        this.mahasiswa = mahasiswa;
        this.start = start;
        this.end = end;
        this.deskripsi = deskripsi;
    }

}
