package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;

import java.text.ParseException;
import java.util.Map;
import java.util.Optional;

public interface LogService {

    Log createLog(String npm, Map<String, String> json) throws ParseException;

    Optional<Log> getLogById(Long idLog);

    Log updateLog(Long idLog, Map<String, String> json) throws ParseException;

    void deleteLogById(Long idLog);

    Iterable<Log> getListLog();

    Iterable<Log> getListLogByNpm(String npm);

    Iterable<Map<String, Object>> getReport(String npm);
}
