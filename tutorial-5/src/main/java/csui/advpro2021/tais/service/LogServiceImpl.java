package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class LogServiceImpl implements LogService{

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    private final int BIAYA_LOG = 350;
    private final String dateTime_pattern = "dd-MM-yyyy HH:mm:ss";
    private final String[] monthList = {
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"};

    ArrayList<Map<String, Object>> report;

    public LogServiceImpl() {
        this.report = new ArrayList<>();
        initializeReport();
    }

    private void initializeReport(){
        for (String s : monthList) {
            HashMap<String, Object> tmpMap = new HashMap<>();
            tmpMap.put("Month", s);
            tmpMap.put("jamKerja", 0.0);
            report.add(tmpMap);
        }
    }

    private Date generateDateTime(String key) throws ParseException {
        Date dateTime = new SimpleDateFormat(dateTime_pattern).parse(key);
        dateTime = addHoursToJavaUtilDate(dateTime, 7);
        return dateTime;
    }

    public Date addHoursToJavaUtilDate(Date date, int hours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, hours);
        return calendar.getTime();
    }

    @Override
    public Log createLog(String npm, Map<String, String> json) throws ParseException {
        Mahasiswa mhs = mahasiswaRepository.findByNpm(npm);

        Date start_date = generateDateTime(json.get("start"));
        Date end_date = generateDateTime(json.get("end"));

        String desc = json.get("deskripsi");
        Log realLog = new Log(mhs, start_date, end_date, desc);
        logRepository.save(realLog);
        return realLog;
    }

    @Override
    public Optional<Log> getLogById(Long idLog) {
        return logRepository.findById(idLog);
    }

    @Override
    public Log updateLog(Long idLog, Map<String, String> json) throws ParseException {
        Log updatedLog = logRepository.findById(idLog).orElse(null);
        if (updatedLog == null) return null;

        Date start_date = generateDateTime(json.get("start"));
        Date end_date = generateDateTime(json.get("end"));

        updatedLog.setStart(start_date);
        updatedLog.setEnd(end_date);
        updatedLog.setDeskripsi(json.get("deskripsi"));
        logRepository.save(updatedLog);

        return updatedLog;
    }

    @Override
    public void deleteLogById(Long idLog) {
        logRepository.deleteById(idLog);
    }

    @Override
    public Iterable<Log> getListLog() {
        return logRepository.findAll();
    }

    public Iterable<Log> getListLogByNpm(String npm) {
        Mahasiswa mhs = mahasiswaRepository.findByNpm(npm);
        if (mhs == null) {
            return null;
        }
        return mhs.getLogList();
    }

    private void generatejamKerja(Iterable<Log> logList){
        Calendar calendar = Calendar.getInstance();
        for (Log log : logList) {
            Date start = log.getStart();
            Date end = log.getEnd();
            double duration = (double)(end.getTime() - start.getTime()) / 3600000.0;
            calendar.setTime(end);
            HashMap<String, Object> tmpMap = (HashMap<String, Object>)report.get(calendar.get(Calendar.MONTH));
            double jamKerja = (double)tmpMap.get("jamKerja") + duration;
            tmpMap.put("jamKerja", jamKerja);
        }
    }

    private void generateGaji(){
        for (int i = 0; i < monthList.length; i++) {
            HashMap<String, Object> tmpMap = (HashMap<String, Object>)report.get(i);
            tmpMap.put("Pembayaran", (double)tmpMap.get("jamKerja") * BIAYA_LOG);
        }
    }

    public Iterable<Map<String, Object>> getReport(String npm) {
        Iterable<Log> logList = getListLogByNpm(npm);
        if (logList == null) {
            return null;
        }
        generatejamKerja(logList);
        generateGaji();
        return report;
    }

}
