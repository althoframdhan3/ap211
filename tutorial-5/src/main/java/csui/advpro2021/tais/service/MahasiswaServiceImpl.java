package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MahasiswaServiceImpl implements MahasiswaService {

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Override
    public Mahasiswa createMahasiswa(Mahasiswa mahasiswa) {
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public Iterable<Mahasiswa> getListMahasiswa() {
        return mahasiswaRepository.findAll();
    }

    @Override
    public Mahasiswa getMahasiswaByNPM(String npm) {
        return mahasiswaRepository.findByNpm(npm);
    }

    @Override
    public Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa) {
//        mahasiswa.setNpm(npm);

        Mahasiswa oldMhs = mahasiswaRepository.findByNpm(npm);
        if (oldMhs==null) return null;
        oldMhs.setNama(mahasiswa.getNama());
        oldMhs.setEmail(mahasiswa.getEmail());
        oldMhs.setIpk(mahasiswa.getIpk());
        oldMhs.setNoTelp(mahasiswa.getNoTelp());

        mahasiswaRepository.save(oldMhs);
        return mahasiswa;
    }

    public MataKuliah setMataKuliah(String npm, String kode_matkul){
        MataKuliah matkulku = mataKuliahRepository.findByKodeMatkul(kode_matkul);
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        if (mahasiswa == null || matkulku == null) return null;
        mahasiswa.setAsdosMatKul(matkulku);
        mahasiswaRepository.save(mahasiswa);
        return matkulku;
    }

    @Override
    public void deleteMahasiswaByNPM(String npm) {
        Mahasiswa mhs = mahasiswaRepository.findByNpm(npm);
        if (mhs != null && mhs.getAsdosMatKul()==null){
            mahasiswaRepository.deleteById(npm);
        }

    }
}
