- Database requirements <br>
  - [x] **Mahasiswa**-**MataKuliah** (Many-to-One) Relationship
  - [x] Menghubungkan relasi antara **Mahasiswa** dan **MataKuliah**
    
- **Mahasiswa**
  - [x] dapat mendaftar ke satu **MataKuliah**
  - [x] **Mahasiswa**(on_delete=PROTECT, gaji=350/jam) ketika sudah menjadi asdos
  - [x] dapat CRUD **Log** yang sudah dibuat
  - [x] dapat melihat laporan pembayaran untuk bulan tertentu (return semua **Log** pekerjaan dan jumlah uang)
  - [x] laporan pembayaran: data bulan, jam kerja dengan satuan jam, pembayaran yang didapatkan.
    
- **MataKuliah**
  - [x] **MataKuliah**(on_delete=PROTECT) ketika memiliki asdos

- **Log**
  - [x] mencatat pekerjaan **Mahasiswa** yg menjadi asdos
  - [x] dapat saja berisikan data mahasiswa kurang dari satu jam
    