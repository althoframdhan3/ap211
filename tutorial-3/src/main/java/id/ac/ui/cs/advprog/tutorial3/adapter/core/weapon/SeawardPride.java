package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class SeawardPride implements Weapon {


    private String holderName;

    public SeawardPride(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        // complete me
        return "SeawardPride normal attack";
    }

    @Override
    public String chargedAttack() {
        // complete me
        return "SeawardPride charged attack";
    }

    @Override
    public String getName() {
        return "SeawardPride";
    }

    @Override
    public String getHolderName() { return holderName; }
}
