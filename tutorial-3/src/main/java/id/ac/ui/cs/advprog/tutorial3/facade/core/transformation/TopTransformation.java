package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

import java.util.ArrayList;
import java.util.List;

public class TopTransformation {

    Codex runicCodex;
    Codex alphaCodex;
    AbyssalTransformation abyssal;
    CelestialTransformation celestial;
    private List<Transformation> steps;

    public TopTransformation(){
        runicCodex = RunicCodex.getInstance();
        alphaCodex = AlphaCodex.getInstance();
        steps = new ArrayList<>();
        steps.add(new CelestialTransformation());
        steps.add(new AbyssalTransformation());
    }

    public String encode(String text){
        Spell spell = new Spell(text, alphaCodex);

        for (Transformation step:steps) {
            spell = step.encode(spell);
        }
        Spell translatedSpell = CodexTranslator.translate(spell, runicCodex);
        return translatedSpell.getText();
    }

    public String decode(String code){
        Spell spell = new Spell(code, runicCodex);

        for (int i = steps.size()-1; i >= 0; i--) {
            spell = steps.get(i).decode(spell);
        }

        Spell translatedSpell = CodexTranslator.translate(spell, alphaCodex);
        return translatedSpell.getText();
    }
}
