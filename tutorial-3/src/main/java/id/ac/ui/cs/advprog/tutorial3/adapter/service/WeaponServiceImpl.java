package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    // implement me
    @Override
    public List<Weapon> findAll() {
        for (Bow bow: bowRepository.findAll()) {
            weaponRepository.save(new BowAdapter(bow));
        }
        for (Spellbook spell: spellbookRepository.findAll()) {
            weaponRepository.save(new SpellbookAdapter(spell));
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        String temp = weapon.getHolderName()+" attacked with "+weapon.getName();
        if (attackType == 0) {
            logRepository.addLog(temp+" (normal attack): "+weapon.normalAttack());
        } else {
            logRepository.addLog(temp+" (charged attack): "+weapon.chargedAttack());
        }
        weaponRepository.save(weapon);
    }

    // implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }

}
