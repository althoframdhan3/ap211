package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// complete me :)
public class BowAdapter implements Weapon {

    private final Bow bow;
    public boolean isAim;

    public BowAdapter(Bow bow){
        this.bow = bow;
        this.isAim = false;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(isAim);
    }

    @Override
    public String chargedAttack() {
        this.isAim = !isAim;
        return isAim ? "Entering aim shot mode" : "Leaving aim shot mode";
    }

    @Override
    public String getName() {
        return bow.getName();

    }

    @Override
    public String getHolderName() {
        // complete me
        return bow.getHolderName();
    }

}
