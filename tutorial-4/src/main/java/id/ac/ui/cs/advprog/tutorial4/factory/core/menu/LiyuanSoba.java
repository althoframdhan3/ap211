package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public class LiyuanSoba extends Menu {
    private static final Noodle noodle = new Soba();
    private static final Meat meat = new Beef();
    private static final Topping topping = new Mushroom();
    private static final Flavor flavor = new Sweet();

    //Ingridients:
    //Noodle: Soba
    //Meat: Beef
    //Topping: Mushroom
    //Flavor: Sweet
    public LiyuanSoba(String name){ super(name, noodle, meat, topping, flavor); }
}