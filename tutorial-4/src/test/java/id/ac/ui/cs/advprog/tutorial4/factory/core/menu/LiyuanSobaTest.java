package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LiyuanSobaTest {
    Menu liyuba;

    @BeforeEach
    public void setUp() throws Exception {
//        bowAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter");
//        bowClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow");
        liyuba = new LiyuanSoba("lyuansoba");
    }

    @Test
    public void testConstructor(){
        assertEquals(liyuba, liyuba);
    }

    @Test
    public void testGetName(){
        assertEquals("lyuansoba", liyuba.getName());
    }
}