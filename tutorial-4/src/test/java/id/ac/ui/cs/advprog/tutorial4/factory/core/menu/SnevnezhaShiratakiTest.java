package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SnevnezhaShiratakiTest {
    Menu shirataki;

    @BeforeEach
    public void setUp() throws Exception {
//        bowAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter");
//        bowClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow");
        shirataki = new SnevnezhaShirataki("Snevnezha");
    }

    @Test
    public void testConstructor(){
        assertEquals(shirataki, shirataki);
    }

    @Test
    public void testGetName(){
        assertEquals("Snevnezha", shirataki.getName());
    }
}