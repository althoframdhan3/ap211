package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MushroomTest {
    @Test
    public void testGetDescription() throws Exception {
        Topping mushroom = new Mushroom();
        assertEquals("Adding Shiitake Mushroom Topping...",mushroom.getDescription());
    }
}