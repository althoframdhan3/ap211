package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class ChickenTest {
    private Class<?> chickenMeatClass;
    Flavor flavor;

    @Test
    public void testGetDescription() throws Exception {
        Meat chicken = new Chicken();
        assertEquals("Adding Wintervale Chicken Meat...",chicken.getDescription());
    }
}