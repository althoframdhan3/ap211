package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RamenTest {
    @Test
    public void testGetDescription() throws Exception {
        Noodle ramen = new Ramen();
        assertEquals("Adding Inuzuma Ramen Noodles...",ramen.getDescription());
    }
}