package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SaltyTest {
    private Class<?> saltyFlavorClass;
    Flavor flavor;

    @BeforeEach
    public void setUp() throws Exception {
        saltyFlavorClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty");
    }

    @Test
    public void testIonicBowIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(saltyFlavorClass.getModifiers()));
    }

    @Test
    public void testSaltyIsAnFlavorBehavior() {
        Collection<Type> interfaces = Arrays.asList(saltyFlavorClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testGetDescription() throws Exception {
        Flavor salty = new Salty();
        assertEquals("Adding a pinch of salt...",salty.getDescription());
    }
}