package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class BeefTest {
    private Class<?> beefMeatClass;
    Flavor flavor;

    @Test
    public void testGetDescription() throws Exception {
        Meat beef = new Beef();
        assertEquals("Adding Maro Beef Meat...",beef.getDescription());
    }
}