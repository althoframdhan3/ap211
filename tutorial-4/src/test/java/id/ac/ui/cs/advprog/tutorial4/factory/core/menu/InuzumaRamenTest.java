package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InuzumaRamenTest {
    Menu inuzumaramen;

    @BeforeEach
    public void setUp() throws Exception {
//        bowAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter");
//        bowClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow");
        inuzumaramen = new InuzumaRamen("inuzumaramen");
    }

    @Test
    public void testConstructor(){
        assertEquals(inuzumaramen, inuzumaramen);
    }

    @Test
    public void testGetName(){
        assertEquals("inuzumaramen", inuzumaramen.getName());
    }
}