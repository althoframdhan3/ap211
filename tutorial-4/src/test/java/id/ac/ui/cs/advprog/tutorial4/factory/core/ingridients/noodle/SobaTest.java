package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SobaTest {
    @Test
    public void testGetDescription() throws Exception {
        Noodle soba = new Soba();
        assertEquals("Adding Liyuan Soba Noodles...",soba.getDescription());
    }
}