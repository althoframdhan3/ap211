package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MenuTest {
    Menu menu;
    private static final Noodle noodle = new Soba();
    private static final Meat meat = new Beef();
    private static final Topping topping = new Mushroom();
    private static final Flavor flavor = new Sweet();

    @BeforeEach
    public void setUp() throws Exception {
//        bowAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter");
//        bowClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow");
        menu = new Menu("menu1",noodle, meat, topping, flavor);
    }

    @Test
    public void testConstructor(){
        assertEquals(menu, menu);
    }

    @Test
    public void testGetName(){
        assertEquals("menu1", menu.getName());
    }

    @Test
    public void testGetFlavor(){
        assertEquals(flavor, menu.getFlavor());
    }

    @Test
    public void testGetTopping(){
        assertEquals(topping, menu.getTopping());
    }

    @Test
    public void testGetMeat(){
        assertEquals(meat, menu.getMeat());
    }

    @Test
    public void testGetNoodle(){
        assertEquals(noodle, menu.getNoodle());
    }
}