package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MondoUdonTest {
    Menu mondo;

    @BeforeEach
    public void setUp() throws Exception {
//        bowAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter");
//        bowClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow");
        mondo = new MondoUdon("mondoudon");
    }

    @Test
    public void testConstructor(){
        assertEquals(mondo, mondo);
    }

    @Test
    public void testGetName(){
        assertEquals("mondoudon", mondo.getName());
    }
}