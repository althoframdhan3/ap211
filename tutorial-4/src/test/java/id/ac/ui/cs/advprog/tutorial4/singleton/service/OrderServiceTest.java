package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import id.ac.ui.cs.advprog.tutorial4.factory.service.MenuService;
import id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {
    private Class<?> orderServiceClass;

//    @Mock
//    MenuRepository menuRepo;

    @Spy
    OrderServiceImpl orderService = new OrderServiceImpl();

    @InjectMocks
    OrderService orderServiceimpl = new OrderServiceImpl();

    @BeforeEach
    public void setup() throws Exception {
        orderServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
    }

    @Test
    public void testMenuServiceGetFoodCorrectlyImplemented() {
        orderServiceimpl.orderAFood("nasi");
        assertEquals("nasi", orderServiceimpl.getFood().toString());
    }

    @Test
    public void testMenuServiceGetDrinkCorrectlyImplemented() {
        orderServiceimpl.orderADrink("boba");
        assertEquals("boba", orderServiceimpl.getDrink().toString());
    }
}