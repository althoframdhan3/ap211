package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UdonTest {
    @Test
    public void testGetDescription() throws Exception {
        Noodle udon = new Udon();
        assertEquals("Adding Mondo Udon Noodles...",udon.getDescription());
    }
}