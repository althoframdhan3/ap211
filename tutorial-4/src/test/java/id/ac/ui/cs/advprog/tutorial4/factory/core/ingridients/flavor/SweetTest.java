package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;


import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SweetTest {
    private Class<?> sweetFlavorClass;
    Flavor flavor;

    @Test
    public void testGetDescription() throws Exception {
        Flavor sweet = new Sweet();
        assertEquals("Adding a dash of Sweet Soy Sauce...",sweet.getDescription());
    }
}