package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShiratakiTest {
    @Test
    public void testGetDescription() throws Exception {
        Noodle shirataki = new Shirataki();
        assertEquals("Adding Snevnezha Shirataki Noodles...",shirataki.getDescription());
    }
}