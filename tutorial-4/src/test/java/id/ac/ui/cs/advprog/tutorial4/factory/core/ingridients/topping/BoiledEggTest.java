package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoiledEggTest {
    @Test
    public void testGetDescription() throws Exception {
        Topping biolegg = new BoiledEgg();
        assertEquals("Adding Guahuan Boiled Egg Topping",biolegg.getDescription());
    }
}