package id.ac.ui.cs.advprog.tutorial4.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;


import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OrderTest {

    private Class<?> orderDrinkClass;
    private OrderDrink orderDrink;

    private Class<?> orderFoodClass;
    private OrderFood orderFood;

    @BeforeEach
    public void setUp() throws Exception {
        orderDrinkClass = Class.forName(OrderDrink.class.getName());

        orderFoodClass = Class.forName(OrderFood.class.getName());
    }

    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(orderDrinkClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertFalse(check);
    }

    @Test
    public void testNoPublicConstructors2() {
        List<Constructor> constructors = Arrays.asList(orderFoodClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertFalse(check);
    }

//    @Test
//    public void testOrderFoodEagerInstantiation(){
//
//        assertNotNull(OrderFood.getObj());
//    }
//
//    @Test
//    public void testOrderDrinkLazyInstantiation(){
//
//        assertNull(OrderDrink.getObj());
//    }

    @Test
    public void testGetInstanceShouldReturnSingletonInstance() {
        OrderDrink orderDrink = OrderDrink.getInstance();

        assertNotNull(orderDrink);
    }

    @Test
    public void testGetInstanceShouldReturnSingletonInstance2() {
        OrderFood orderFood = OrderFood.getInstance();

        assertNotNull(orderFood);
    }

    @Test
    public void testOrderDrink(){
        OrderDrink drink = OrderDrink.getInstance();
        drink.setDrink("Bir Bintang");
        assertEquals("Bir Bintang", drink.getDrink());
    }

    @Test
    public void testOrderFood(){
        OrderFood food = OrderFood.getInstance();
        food.setFood("Tonkotsu Ramen");
        assertEquals("Tonkotsu Ramen", food.getFood());
    }

    @Test
    public void testOrderDrinkExist(){
        OrderDrink drink = OrderDrink.getInstance();
        drink.setDrink("boba");
        assertNotNull(drink.toString());
    }

    @Test
    public void testOrderFoodExist(){
        OrderFood food = OrderFood.getInstance();
        food.setFood("nasi");
        assertNotNull(food.toString());
    }



}
