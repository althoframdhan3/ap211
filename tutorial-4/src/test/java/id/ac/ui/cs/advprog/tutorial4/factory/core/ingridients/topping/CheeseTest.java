package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CheeseTest {
    @Test
    public void testGetDescription() throws Exception {
        Topping cheese = new Cheese();
        assertEquals("Adding Shredded Cheese Topping...",cheese.getDescription());
    }
}