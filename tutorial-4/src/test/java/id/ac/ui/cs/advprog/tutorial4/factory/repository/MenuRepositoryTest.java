package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class MenuRepositoryTest {
    MenuRepository repo;

    @BeforeEach
    public void setUp() throws Exception {
//        bowAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter");
//        bowClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow");
        repo = new MenuRepository();
    }

    @Test
    public void testMenuRepoList(){
        assertTrue(repo.getMenus() instanceof ArrayList);
    }

    @Test
    public void testAddMenuList(){
        Menu menu1 = new Menu("menu1",new Soba(), new Beef(), new Mushroom(), new Sweet());
        Menu newmenu = repo.add(menu1);
        assertEquals(1, repo.getMenus().size());
        assertEquals(newmenu, menu1);
    }
}