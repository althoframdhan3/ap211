package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class MenuServiceTest {
    private Class<?> menuServiceClass;

    @Mock
    MenuRepository menuRepo;

    @Spy
    MenuServiceImpl menuService = new MenuServiceImpl();

    @InjectMocks
    MenuService weaponService = new MenuServiceImpl();

    @BeforeEach
    public void setup() throws Exception {
        menuServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
    }

    @Test
    public void testMenuServiceGetAllMenusCorrectlyImplemented() {
        weaponService.getMenus();
        verify(menuRepo, atLeastOnce()).getMenus();
    }
}