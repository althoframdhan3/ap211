package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.ArrayList;
import java.util.List;

public class KnightAdventurer extends Adventurer {

    private List<Quest> quests = new ArrayList<>();

    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        this.guild = guild;
        guild.add(this);

    }

    @Override
    public void update() {
        quests.add(this.guild.getQuest());
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public List<Quest> getQuests() {
        return this.quests;
    }


}
