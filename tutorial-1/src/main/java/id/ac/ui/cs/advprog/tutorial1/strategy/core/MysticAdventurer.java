package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {

    private AttackBehavior attackBehavior;
    private DefenseBehavior defenseBehavior;
    String alias;

    public MysticAdventurer (){
        this.alias = "Mystic";
        this.attackBehavior = new AttackWithMagic();
        this.defenseBehavior = new DefendWithShield();
    }

    @Override
    public String getAlias() {
        return alias;
    }

    public void setAttackBehavior(AttackBehavior attackBehavior) {
        this.attackBehavior = attackBehavior;
    }

    public AttackBehavior getAttackBehavior() {
        return attackBehavior;
    }

    public void setDefenseBehavior(DefenseBehavior defenseBehavior) {
        this.defenseBehavior = defenseBehavior;
    }

    public DefenseBehavior getDefenseBehavior() {
        return defenseBehavior;
    }

}
