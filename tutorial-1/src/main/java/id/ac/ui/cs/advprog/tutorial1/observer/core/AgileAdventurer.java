package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.ArrayList;
import java.util.List;

public class AgileAdventurer extends Adventurer {

    private List<Quest> quests = new ArrayList<>();

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
        this.guild.add(this);

    }

    @Override
    public void update() {
        if (!this.guild.getQuestType().equals("Escort")) {
            quests.add(this.guild.getQuest());
        }
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public List<Quest> getQuests() {
        return this.quests;
    }


}
