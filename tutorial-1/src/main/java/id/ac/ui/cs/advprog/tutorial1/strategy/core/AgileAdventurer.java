package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {


    AttackBehavior attackBehavior;
    DefenseBehavior defenseBehavior;
    String alias;

    public AgileAdventurer (){
        this.alias = "Agile";
        this.attackBehavior = new AttackWithGun();
        this.defenseBehavior = new DefendWithBarrier();
    }

    @Override
    public String getAlias() {
        return alias;
    }

    public void setAttackBehavior(AttackBehavior attackBehavior) {
        this.attackBehavior = attackBehavior;
    }

    public AttackBehavior getAttackBehavior() {
        return attackBehavior;
    }

    public void setDefenseBehavior(DefenseBehavior defenseBehavior) {
        this.defenseBehavior = defenseBehavior;
    }

    public DefenseBehavior getDefenseBehavior() {
        return defenseBehavior;
    }

}
