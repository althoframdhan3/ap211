package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    @Override
    public String attack() {
        return "DUORRR \uD83D\uDD2B";
    }

    @Override
    public String getType() {
        return "GUN";
    }

}
