package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    @Override
    public String defend() {
        return "SHIELD ACTIVATED \uD83D\uDEE1";
    }

    @Override
    public String getType() {
        return "shield";
    }

}
