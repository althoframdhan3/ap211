package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {

    AttackBehavior attackBehavior;
    DefenseBehavior defenseBehavior;
    String alias;

    public KnightAdventurer(){
        this.alias = "Knight";
        this.attackBehavior = new AttackWithSword();
        this.defenseBehavior = new DefendWithArmor();
    }

    @Override
    public String getAlias() {
        return alias;
    }

    public void setAttackBehavior(AttackBehavior attackBehavior) {
        this.attackBehavior = attackBehavior;
    }

    public AttackBehavior getAttackBehavior() {
        return attackBehavior;
    }

    public void setDefenseBehavior(DefenseBehavior defenseBehavior) {
        this.defenseBehavior = defenseBehavior;
    }

    public DefenseBehavior getDefenseBehavior() {
        return defenseBehavior;
    }

}
