package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.ArrayList;
import java.util.List;

public class MysticAdventurer extends Adventurer {

    private List<Quest> quests = new ArrayList<>();

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
        this.guild.add(this);

    }

    @Override
    public void update() {
        if (!this.guild.getQuestType().equals("Rumble")) {
            quests.add(this.guild.getQuest());
        }
    }

    @Override
    public List<Quest> getQuests() {
        return this.quests;
    }


}
